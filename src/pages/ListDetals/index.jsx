import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import ListItem from "../../components/ListItem";
import Navbar from "../../components/Navbar";
import useDataFetching from "../../hooks/useDataFetching";

const dataSource = 'https://my-json-server.typicode.com/PacktPublishing/React-Projects-Second-Edition/items/'

const ListDetailWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  margin: 2% 5%;
`

function ListDetail() {
  const [items, setItems] = useState([])
  const navigate = useNavigate()
  const { listId } = useParams()

  const [loading, data, error] = useDataFetching(dataSource)

  useEffect(() => {
    data && listId && setItems((data.filter((item) => item.listId === parseInt(listId))))
  }, [data, listId]);

  return (
    <>
      {
        navigate && (
          <Navbar 
            goBack={() => navigate(-1)} 
            openForm={() => navigate(`/list/${listId}/new`)} 
          />
        )
      }
      <ListDetailWrapper>
        {
          loading ? (
            <span>Loading...</span>
          ) : (
            items.map((item) => <ListItem key={item.id} data={item} />)
          )
        }
      </ListDetailWrapper>
    </>
  )
}

export default ListDetail