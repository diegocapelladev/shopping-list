import { Link, useNavigate } from "react-router-dom"
import styled from "styled-components"
import Navbar from "../../components/Navbar"
import useDataFetching from "../../hooks/useDataFetching"

const dataSource = 'https://my-json-server.typicode.com/PacktPublishing/React-Projects-Second-Edition/lists'

const ListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin: 5%;
`

const ListLink = styled(Link)`
  display: flex;
  text-align: left;
  align-items: center;
  padding: 1%;
  background: lightgray;
  border-radius: 5px;
  padding: 10px;
  margin-bottom: 2%;
  color: black;
  text-decoration: none;
`

const Title = styled.h3`
  flex-basis: 80%;
`

function Lists() {
  const navigate = useNavigate()

  const [loading, data, error] = useDataFetching(dataSource)

  return (
    <>
      {navigate && <Navbar title="Your Lists" />}
      <ListWrapper>
        {
          loading ? (
            <span>{ 'Loading...'}</span>
          ) : (
            data.map((list) => (
              <ListLink key={list.id} to={`list/${list.id}`}>
                <Title>{list.title}</Title>
              </ListLink>
            ))
          )
        }
        
      </ListWrapper>
    </>
  )
}

export default Lists