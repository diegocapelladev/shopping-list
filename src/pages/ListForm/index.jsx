import { useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import Button from '../../components/Button'
import FormItem from "../../components/FormItem";
import Navbar from "../../components/Navbar";

const ListFormWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  margin: 2% 5%;
`

const SubmitButton = styled(Button)`
  background: blue;
  margin: 2% 0;
`

function ListForm() {
  const navigate = useNavigate()
  const { listId } = useParams()

  return (
    <>
      {navigate && <Navbar goBack={() => navigate(-1)} title="Add Item" />}
      <ListFormWrapper>
        <form>
          <FormItem id="title" label="Title" placeholder="Insert title" />
          <FormItem 
            id="quantity"
            label="Quantity"
            type="number"
            placeholder="0"
          />
          <FormItem id="price" label="Price" type="number" placeholder="0.00" />
          <SubmitButton>Add Item</SubmitButton>
        </form>
      </ListFormWrapper>
    </>
  )
}

export default ListForm