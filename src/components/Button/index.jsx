import styled from "styled-components"

 const Button = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  background: orange;
  color: white;
  padding: 10px;
  line-height: 2;
  border-radius: 5px;
  border: 0;
  font-size: inherit;
  cursor: pointer;
`
export default Button