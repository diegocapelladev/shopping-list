import styled from "styled-components";

const HeaderWrapper = styled.div`
  background: orange;
  height: 100%;
  color: white;
  font-size: calc(10px + 2vmin);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const Title = styled.h1`
  pointer-events: none;
`

function Header() {
  return (
    <HeaderWrapper>
      <Title>Personal Shopping List</Title>
    </HeaderWrapper>
  )
}

export default Header