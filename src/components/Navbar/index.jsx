import styled from "styled-components"

const NavbarWrapper = styled.nav`
  width: 100%;
  display: flex;
  /* align-items: center; */
  justify-content: space-between;
  background: navajowhite;
`

const Title = styled.h2`
  text-align: center;
  flex-basis: 60%;
  &:first-child {
    margin-left: 20%;
  }
  &:last-child {
    margin-right: 20%;
  }
`

const NavbarButton = styled.button`
  margin: 10px 5%;
`

function Navbar({ goBack, title, openForm }) {
  return (
    <NavbarWrapper>
      { goBack && <NavbarButton onClick={goBack}>{`< Go Back`}</NavbarButton> }
      <Title>{title}</Title>
      {
        openForm && <NavbarButton onClick={openForm} >{`+ Add Item`}</NavbarButton>
      }
    </NavbarWrapper>
  )
}

export default Navbar