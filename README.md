# Projeto Shopping List React Hooks

## Get starting

`npm install` or `yarn install`

## Run

`npm run dev` or `yarn dev`

## Final App

<img src="./src/assets/shopping-list.png" />